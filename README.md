# PCB_Pizero_Scan2DXF

Petite PCB créée pour faciliter les petites expérimentations et l'usage du script scan2dxf utilisé en atelier. 1 Bouton et 2 Leds, plus l'accès à l'I2C, quelques GPIO et les broches d'alimentation. Ce projet est dessiné avec Kicad puis les parcours d'outils ont été générés à l'aide de Flatcam. 
La réalisation de ce projet va faire l'objet d'une documentation poussée sur notre wiki.

![preview](pics/IMG_20190802_180423.jpg "Preview")
![pinout](pics/pinout.png "pinout")